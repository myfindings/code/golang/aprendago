# aprendaGO

#### [cap1.visao-geral-1-bem-vindo](http://example.com "Title"). ####
#### [cap1.visao-geral-2-por-que-go](http://example.com "Title"). ####

- cap1.visao-geral-3-sucesso
- cap1.visao-geral-4-recursos
- cap1.visao-geral-5-como-esse-curso-funciona
- cap1.visao-geral-6-update-exercicios

- cap2.variaveis-valores-tipos-1-go-playground
- cap2.variaveis-valores-tipos-2-hello-world
- cap2.variaveis-valores-tipos-3-operador-curto-de-declaracao
- cap2.variaveis-valores-tipos-4-a-palavra-chave-var
- cap2.variaveis-valores-tipos-5-explorando-tipos
- cap2.variaveis-valores-tipos-6-valor-zero
- cap2.variaveis-valores-tipos-7-o-pacote-fmt
- cap2.variaveis-valores-tipos-8-criando-seu-proprio-tipo
- cap2.variaveis-valores-tipos-9-conversao-nao-coercao

cap3.exercicios-nivel-1-1
cap3.exercicios-nivel-1-2
cap3.exercicios-nivel-1-3
cap3.exercicios-nivel-1-4
cap3.exercicios-nivel-1-5
cap3.exercicios-nivel-1-6
cap4.fundamentos-da-programacao-1-tipo-booleano
cap4.fundamentos-da-programacao-2-como-os-computadores-funcionam
cap4.fundamentos-da-programacao-3-tipos-numericos
cap4.fundamentos-da-programacao-4-overflow
cap4.fundamentos-da-programacao-5-tipo-string-cadeias-de-caracteres
cap4.fundamentos-da-programacao-6-sistemas-numericos
cap4.fundamentos-da-programacao-7-constantes
cap4.fundamentos-da-programacao-8-Iota
cap4.fundamentos-da-programacao-9-deslocamento-de-bits
cap5.exercicios-nivel-2-1
cap5.exercicios-nivel-2-2
cap5.exercicios-nivel-2-3
cap5.exercicios-nivel-2-4
cap5.exercicios-nivel-2-5
cap5.exercicios-nivel-2-6
cap5.exercicios-nivel-2-7
cap6.fluxo-de-controle-1-entendendo-fluxo-de-controle
cap6.fluxo-de-controle-2-loops-inicializacao-condicao-pos
cap6.fluxo-de-controle-3-loops-nested-loop-repeticao-hierarquica
cap6.fluxo-de-controle-4-loops-a-declaracao-for
cap6.fluxo-de-controle-5-loops-break-e-continue
cap6.fluxo-de-controle-6-loops-utilizando-ascii
cap6.fluxo-de-controle-7-condicionais-a-declaracao-if
cap6.fluxo-de-controle-8-condicionais-if-elseif-else
cap6.fluxo-de-controle-9-condicionais-a-declaracao-switch
cap6.fluxo-de-controle-10-condicionais-a-declaracao-switch-pt2-documentacao
cap6.fluxo-de-controle-11-operadores-logicos-condicionais
cap7.exercicios-nivel-3-1
cap7.exercicios-nivel-3-2
cap7.exercicios-nivel-3-3
cap7.exercicios-nivel-3-4
cap7.exercicios-nivel-3-5
cap7.exercicios-nivel-3-6
cap7.exercicios-nivel-3-7
cap7.exercicios-nivel-3-8
cap7.exercicios-nivel-3-9
cap7.exercicios-nivel-3-10
cap8-agrupamentos-de-dados-2-slice-literal-composta
cap8-agrupamentos-de-dados-3-slice-for-range
cap8-agrupamentos-de-dados-4-slice-fatiando-ou-deletando-de-uma-fatia
cap8-agrupamentos-de-dados-5-slice-anexando-a-uma-slice
cap8-agrupamentos-de-dados-6-slice-make
cap8-agrupamentos-de-dados-7-slice-slice-multi-dimensional
cap8-agrupamentos-de-dados-8-slice-a-surpresa-do-array-subjacente
cap8-agrupamentos-de-dados-9-maps-introducao
cap8-agrupamentos-de-dados-9-maps-range-deletando
cap9.exercicios-nivel-4-1
cap9.exercicios-nivel-4-2
cap9.exercicios-nivel-4-3
cap9.exercicios-nivel-4-4
cap9.exercicios-nivel-4-5
cap9.exercicios-nivel-4-6
cap9.exercicios-nivel-4-7
cap9.exercicios-nivel-4-8
cap9.exercicios-nivel-4-9
cap9.exercicios-nivel-4-10
cap10.structs-1-struct
cap10.structs-2-structs-embutidos
cap10.structs-3-lendo-a-documentacao
cap10.structs-3-structs-anonimos
cap11.exercicios-nivel-5-1
cap11.exercicios-nivel-5-2
cap11.exercicios-nivel-5-3
cap11.exercicios-nivel-5-4
cap12.funcoes-1-sintaxe
cap12.funcoes-2-desenrolando-enumerando-uma-slice
cap12.funcoes-3-defer
cap12.funcoes-4-metodos
cap12.funcoes-5-interfaces-polimorfismo
cap12.funcoes-6-funcoes-anonimas
cap12.funcoes-7-func-como-expressao
cap12.funcoes-8-retomando-uma-funcao
cap12.funcoes-9-callback
cap12.funcoes-10-closure
cap12.funcoes-11-recursividade
cap13.exercicios-nivel-6-1
cap13.exercicios-nivel-6-2
cap13.exercicios-nivel-6-3
cap13.exercicios-nivel-6-4
cap13.exercicios-nivel-6-5
cap13.exercicios-nivel-6-6
cap13.exercicios-nivel-6-7
cap13.exercicios-nivel-6-8
cap13.exercicios-nivel-6-9
cap13.exercicios-nivel-6-10
cap13.exercicios-nivel-6-11
cap14.ponteiros-1-o-que-sao-ponteiros
cap14.ponteiros-2-quando-usar-ponteiros
cap15.exercicios-nivel-7-1
cap15.exercicios-nivel-7-2
cap16.aplicacoes-1-documentacao-json
cap16.aplicacoes-2-json-marshal-ordenacao
cap16.aplicacoes-3-json-unmarshal-desordenacao
cap16.aplicacoes-4-a-interface-writer
cap16.aplicacoes-5-o-pacote-sort
cap16.aplicacoes-6-customizando-o-sort
cap17.exercicios-nivel-8-1
cap17.exercicios-nivel-8-2
cap17.exercicios-nivel-8-3
cap17.exercicios-nivel-8-4
cap17.exercicios-nivel-8-5
cap18.concorrencia-1-concorrencia-vs-paralelismo
cap18.concorrencia-2-goroutines-waitgroups
cap18.concorrencia-3-discussao-condicao-de-corrida
cap18.concorrencia-4-na-pratica-condicao-de-corrida
cap18.concorrencia-5-mutex
cap18.concorrencia-6-atomic
cap19.ambiente-de-desenvolvimento-1-o-terminal
cap19.ambiente-de-desenvolvimento-2-go-workspace
cap19.ambiente-de-desenvolvimento-3-ides
cap19.ambiente-de-desenvolvimento-4-comandos-go
cap19.ambiente-de-desenvolvimento-5-repositorios-do-github
cap19.ambiente-de-desenvolvimento-6-explorando-o-github
cap19.ambiente-de-desenvolvimento-7-compilacao-cruzada
cap19.ambiente-de-desenvolvimento-8-pacotes
cap20.exercicios-nivel-9-1
cap20.exercicios-nivel-9-2
cap20.exercicios-nivel-9-3
cap20.exercicios-nivel-9-4
cap20.exercicios-nivel-9-5
cap20.exercicios-nivel-9-6
cap20.exercicios-nivel-9-7
cap21.canais-1-entendendo-canais
cap21.canais-2-canais-direcionais-utilizando-canais
cap21.canais-3-range-e-close
cap21.canais-4-select
cap21.canais-5-a-expressao-comma-ok
cap21.canais-6-convergencia
cap21.canais-7-divergencia
cap21.canais-8-context
cap22.exercicios-nivel-10-1
cap22.exercicios-nivel-10-2
cap22.exercicios-nivel-10-3
cap22.exercicios-nivel-10-4
cap22.exercicios-nivel-10-5
cap22.exercicios-nivel-10-6
cap22.exercicios-nivel-10-7
cap23.tratamento-de-erros-1-entendendo-erros
cap23.tratamento-de-erros-2-verificando-erros
cap23.tratamento-de-erros-3-print-log
cap23.tratamento-de-erros-4-recover
cap23.tratamento-de-erros-5-erros-com-informacoes-adicionais
cap24.exercicios-nivel-11-1
cap24.exercicios-nivel-11-2
cap24.exercicios-nivel-11-3
cap24.exercicios-nivel-11-4
cap24.exercicios-nivel-11-5
cap25.documentacao-1-introducao
cap25.documentacao-2-godoc
cap25.documentacao-3-godoc
cap25.documentacao-4-godoc
cap25.documentacao-5-escrevendo-documentacao
cap26.exercicios-nivel-12-1
cap26.exercicios-nivel-12-2
cap26.exercicios-nivel-12-3
cap27.testes-benchmarks-1-introducao
cap27.testes-benchmarks-2-testes-em-tabela
cap27.testes-benchmarks-3-testes-como-exemplos
cap27.testes-benchmarks-4-go-fmt-govet-e-golint
cap27.testes-benchmarks-5-benchmarks
cap27.testes-benchmarks-6-cobertura
